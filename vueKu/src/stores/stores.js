import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 1
  },
  getters: {
    counters: state => state.count * 2
  },
  mutations: {
    // MUTaTIONS digunakan untuk merubah value yg berada di state secara tetap

    increment: state => {
      state.count++
    },
    decrement: state => {
      state.count--
    }
  }
})
